# Estória #
Busca por descrição
O usuário irá poder buscar o produto desejado pela descrição ou nome do produto.
Busca por Código de barra
O usuário irá poder buscar o produto pelo código de barras único.
Recomendação de produtos com base no perfil de compra
Com base no perfil do usuário é possível gerar recomendações de melhores produtos para o segmento no qual ele faz parte.
Descoberta de rota para chegar ao melhor local para o usuário
O usuário pode gerar a rota de onde está até o local que tem o produto desejado, também é possível que ele veja as opções de transporte para o local, já podendo solicitar uma viagem.
Compartilhamento de ofertas
O usuário pode compartilhar o preço de um produto em um determinado local através do WhatsApp.
Monitoramento de preços
O usuário poderá monitorar o preço de um determinado produto com o intuito de comprar quando ele for menor que determinado valor. 
Favoritar produtos
O usuário poderá favoritar produtos para melhorar a recomendação de produtos e terá como acompanhar o preço desse produto apenas com um clique.
